# projet-angular ISN 36
By Abdoul, Abiola, Ben and Farida.
## INSTALLATION ET LANCEMENT DU SERVEUR
Après avoir télécharger le projet :

Placer vous dans le repertoire Json-server et lancer la commande
```sh
npm i
```

Lancer le server avec la commande
```sh
json-server --watch data.json
```

## INSTALLATION ET LANCEMENT DU PROJET

Placer vous dans le repertoire projet-angular et lancer la commande
```sh
npm i
```

Lancer le projet avec la commande
```sh
ng serve 
```
et ouvrir la page qui se trouve sur le port 4200 par défaut

```sh
http://localhost:4200/
```