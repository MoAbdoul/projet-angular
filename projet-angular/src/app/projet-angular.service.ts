import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { Personne } from './personne';

@Injectable({
  providedIn: 'root'
})
export class ProjetAngularService {

  private apiUrl = 'http://localhost:3000';

  constructor(private http: HttpClient, private projetAngularService: ProjetAngularService) {

   }

  getAllPersonnes (): Observable<Personne[]>{
    return this.http.get<Personne[]>(this.apiUrl+'/personne')
  }

  getPersonneByCountry (country:String): Observable<Personne[]>{
    return this.http.get<Personne[]>(this.apiUrl+'/personne?pays='+country)
  }

  getStockSector (sectockName:String): Observable<Personne[]>{
    return this.http.get<Personne[]>(this.apiUrl+'/personne?stock_sector='+sectockName)
  }
}



