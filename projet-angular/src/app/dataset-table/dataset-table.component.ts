import { Component, OnInit } from '@angular/core';

import { Personne } from '../personne';
import { ProjetAngularService } from '../projet-angular.service';

@Component({
  selector: 'app-dataset-table',
  templateUrl: './dataset-table.component.html',
  styleUrls: ['./dataset-table.component.scss']
})
export class DatasetTableComponent implements OnInit {
  personnes: Personne[];

  constructor(private projetAngularService : ProjetAngularService) { }

  ngOnInit() {
    this.getPersonnes();
  }

  getPersonnes(): void {
    this.projetAngularService.getAllPersonnes()
    .subscribe(personnes => this.personnes = personnes);
  }
}



