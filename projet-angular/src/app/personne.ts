export class Personne {
    id: number;
    genre: string;
    prenom: string;
    nom: string;
    email: string;
    pays: string;
    ville: string;
    balance: number;
    stock_market: string;
    stock_name: string;
    stock_symbol: string;
    stock_sector: string;
}


