import { TestBed } from '@angular/core/testing';

import { ProjetAngularService } from './projet-angular.service';

describe('ProjetAngularService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProjetAngularService = TestBed.get(ProjetAngularService);
    expect(service).toBeTruthy();
  });
});
