import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { MapComponent } from './map/map.component';
import { DatasetTableComponent } from './dataset-table/dataset-table.component';
import { ChartsComponent } from './charts/charts.component';

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent },
  {path: 'map', component: MapComponent },
  {path: 'charts', component: ChartsComponent },
  {path: 'dataset-table', component: DatasetTableComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
