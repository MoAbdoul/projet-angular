import { Component, OnInit } from '@angular/core';
import * as L from 'leaflet';

// Open Street Map Definition
const layOsm: L.TileLayer = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  maxZoom: 19,
  attribution: 'Map-Name',
  detectRetina: true
});

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  // Values to bind to Leaflet Directive
  leafletOptions: L.MapOptions = {
    zoom: 6,
    center: L.latLng(51.163375, 10.447683)
  };
  baseLayers: {[layerName: string]: L.Layer} = {
    'openstreetmap': layOsm
  };
  layersControlOptions: L.ControlOptions = { position: 'bottomright' };

  constructor() { }

  ngOnInit() { }

  onMapReady(map: L.Map) {
    setTimeout(() => {
      map.invalidateSize();
    }, 0);
 }

}