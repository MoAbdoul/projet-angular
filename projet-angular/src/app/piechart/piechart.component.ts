import { Component, OnInit } from '@angular/core';

import { Personne } from '../personne';
import { ProjetAngularService } from '../projet-angular.service';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './piechart.component.html',
  styleUrls: ['./piechart.component.scss']
})
export class PiechartComponent implements OnInit {

  public chartData: Array<any> ;//= [this.francais.length];//= [20, 30, 40, 50, 60]; , this.americains.length,this.allemands.length, this.espagnols.length, this.anglais.length
  public chartLabels: Array<any> = ['Français', 'Américains', 'Allemands', 'Espagnols', 'Anglais'];

  private francais: Personne[];
  private americains: Personne[];
  private allemands: Personne[];
  private espagnols: Personne[];
  private anglais: Personne[];

  constructor(private projetAngularService: ProjetAngularService) { }

  ngOnInit() {
    this.getPieChartData();
    //this.chartData = [this.francais.length]
  }

  getPieChartData(): void {
    this.projetAngularService.getPersonneByCountry("France")
      .subscribe(personnes => this.francais = personnes);
    this.projetAngularService.getPersonneByCountry("United%20States")
      .subscribe(personnes => this.americains = personnes);
    this.projetAngularService.getPersonneByCountry("Germany")
      .subscribe(personnes => this.allemands = personnes);
    this.projetAngularService.getPersonneByCountry("Spain")
      .subscribe(personnes => this.espagnols = personnes);
    this.projetAngularService.getPersonneByCountry("United%20Kingdom")
      .subscribe(personnes => this.anglais = personnes);
  }

  public chartColors: Array<any> = [{
    hoverBorderColor: ['rgba(0, 0, 0, 0.1)', 'rgba(0, 0, 0, 0.1)', 'rgba(0, 0, 0, 0.1)', 'rgba(0, 0, 0, 0.1)', 'rgba(0, 0, 0, 0.1)'],
    hoverBorderWidth: 0,
    backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
    hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
  }];

  public chartOptions: any = {
    responsive: true
  };
  chartType: string = 'pie';

  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }
}
